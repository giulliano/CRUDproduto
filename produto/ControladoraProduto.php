<?php
	require_once('ColecaoProdutoEmBDR.php');

	class ControladoraProduto{
	
		private $colecao ;
		private $params  ;
		private $geradoraResposta ;
		
		function __construct( GeradoraResposta $geradoraResposta , $params  ){
			$this->geradoraResposta = $geradoraResposta ;
			$this->params = $params ;
			$pdo = new PDO( 'mysql:host=localhost;dbname=comercial', 'root', '');
			$pdow =  new PDOWrapper( $pdo );
			$this->colecao = new ColecaoProdutoEmBDR( $pdow );
		}
		
		function todos(){
			$dtr = new \DataTablesRequest( $this->params );
			$contagem = 0;
			$objetos = array();
			$erro = null ;
			try{
				$contagem = $this->colecao->contagem();
				$objetos = $this->colecao->todos();

			}catch( ColecaoException $e ){
				$erro = $e->getMessage();
			}
			$conteudo = new \DataTablesResponse(
				$contagem ,
				$contagem ,
				$objetos ,
				$dtr->draw(),
				$erro
			);

			return $this->geradoraResposta->ok( $conteudo , GeradoraResposta::TIPO_JSON );
		}
		
		function remover(){
		
			$id = \ParamUtil::value($this->params , 'id' );
			if ( ! is_numeric( $id ) ){
				$msg = " O id informado nao e numerico ";
				$this->geradoraResposta->erro( $msg , GeradoraResposta::TIPO_TEXTO );
			}
			try{
				$this->colecao->remover( $id ) ;
				return $this->geradoraResposta->semConteudo();
			}catch( ColecaoException $e ){
				return $this->geradoraResposta->erro( $msg , GeradoraResposta::TIPO_TEXTO ) ;
			}
		
		}
		
		function atualizar(){
			//primeiro , conferir se todos os campos foram enviados !!!
			$inexistentes = \ArrayUtil::nonExistingKeys( array('id', 'nome' , 'preco'), $this->params );
			if( count( $inexistentes ) > 0 ){
				$msg = "Os seguintes campos nao foram enviados".implode(',' , $inexistentes);
				$this->geradoraRespota->erro( $msg , GeradoraResposta::TIPO_TEXTO ) ;
			}
			$id = \ParamUtil::value( $this->params , 'id');		
			$nome = \ParamUtil::value( $this->params , 'nome');			
			$preco = \ParamUtil::value( $this->params , 'preco');
			$produto = new Produto( $id , $nome , $preco  );
			try{
				$this->colecao->alterar( $produto );
				return $this->geradoraResposta->semConteudo();
			}catch( ColecaoException $e ){
				return $this->geradoraResposta->erro( $msg , GeradoraResposta::TIPO_TEXTO );
			}
		}
		
		function adicionar(){
		//primeiro , conferir se todos os campos foram enviados !!!
			$inexistentes = \ArrayUtil::nonExistingKeys( array('nome' , 'preco'), $this->params );
			if( count( $inexistentes )> 0 ){
				$msg = "Os seguintes campos nao foram informados ".implode(',', $inexistentes );
				return $this->geradoraResposta->erro( $msg , GeradoraResposta::TIPO_TEXTO );
			}
			$nome = \ParamUtil::value( $this->params, 'nome' );
			$preco = \ParamUtil::value( $this->params, 'preco' );
			$produto = new Produto( 0 , $nome , $preco );
			try{
				$this->colecao->adicionar( $produto );
				return $this->geradoraResposta->criado('',GeradoraResposta::TIPO_TEXTO );
			}catch( ColecaoException $e ){
				return $this->geradoraResposta->erro( $e->getMessage() , GeradoraResposta::TIPO_TEXTO );
			}
			
		}
		function comId(){
			$id = \ParamUtil::value($this->params,'id');
			try{
				$produto = $this->colecao->comId( $id );
				return $this->geradoraResposta->ok($produto,GeradoraResposta::TIPO_TEXTO);
			}catch( ColecaoException $e ){
				return $this->geradoraResposta->erro( $e->getMessage() , GeradoraResposta::TIPO_TEXTO );
			}
		}
	}
?>