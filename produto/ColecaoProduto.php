<?php
	require_once( 'ColecaoException.php' );

	interface ColecaoProduto {

		function  adicionar ( &$obj ) ;
		function  remover 	( $id  )  ;
		function  alterar 	( $obj )  ;
		function  comId   	( $id  )  ;
		function  todos     (      )  ;

	}
?>