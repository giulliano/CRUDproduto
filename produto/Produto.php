<?php

	 class Produto {
	
		private $id = 0 ;
		private $preco = 0.0;
		private $nome = "";

		function __construct( $id = 0 , $nome = "" , $preco = 0.0 ){
			
			$this->id = $id ;
			$this->nome = $nome ;
			$this->preco = $preco ;
			

		}
		
		function getNome(){return $this->nome ;}
		function setNome( $nome ){ $this->nome = $nome ; }
	
		function getId(){return $this->id ;}
		function setId( $id ){ $this->id = $id ; }
	
		function getPreco(){return $this->preco ;}
		function setPreco( $preco ){ $this->preco = $preco ; }
	}
?>