<?php
	require_once('Produto.php');
	require_once('ColecaoProduto.php');


	
	class ColecaoProdutoEmBDR implements ColecaoProduto {
		
		const tabela = 'produto';
		
		private $pdow ;
		
		function __construct( PDOWrapper $pdow  ){
			
			$this->pdow = $pdow;
			
		}
		
		function adicionar( &$obj ){
		//validar o obj
			$sql = "INSERT INTO ".self::tabela."( nome , preco ) VALUES( :nome , :preco ) ";
			try{
				$this->pdow->execute( $sql , array(
													"nome"=>  $obj->getNome() ,
													"preco"=> $obj->getPreco() 
												) );
				$obj->setId( $this->pdow->lastInsertId() );
			}catch( Exception $e ){
				throw new ColecaoException( $e->getMessage(), $e->getCode() , $e );
			}
		}
		
		function remover( $id ){
			try{
				$this->pdow->deleteWithId( $id , self::tabela );
			}catch(Exception $e ){
				throw new ColecaoException( $e->getMessage(), $e->getCOde() , $e );
			}
		}
		
		function alterar ( $obj ){
		//validar o obj

			$sql = "UPDATE ".self::tabela." SET nome = :nome , preco = :preco WHERE id = :id";
			try{
				$this->pdow->execute( $sql , array(
													"nome" =>$obj->getNome(),
													"preco"=>$obj->getPreco(),
													"id"   =>$obj->getId()
												  ));
			
			}catch(Exception $e ){
				throw new ColecaoException( $e->getMessage(), $e->getCOde() , $e );
			}
		}
		
	function comId( $id ) {
		try {
			return $this->pdow->objectWithId( array( $this, 'construirObjeto' ), $id, self::tabela );
		} catch ( \Exception $e ) {
			throw new ColecaoException( $e->getMessage(), $e->getCode(), $e );
		}		
	}

		
		function todos(){
			try{
				return $this->pdow->allObjects( array( $this , 'construirObjeto'), self::tabela );
			}catch( Exception $e ){
				throw new ColecaoException( $e->getMessage(), $e->getCOde() , $e );
			}
		}
		
		 function construirObjeto( array $row ){
			return new Produto( $row['id'] , $row['nome'] , $row['preco'] );
		}
		function contagem() {
		try {
			return $this->pdow->countRows( self::tabela );
		} catch ( \Exception $e ) {
			throw new ColecaoException( $e->getMessage(), $e->getCode(), $e );
		}		
	}
		
	}

?>