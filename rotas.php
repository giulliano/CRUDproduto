<?php
	require_once( "vendor\slim\slim\Slim\Slim.php ");
	require_once("ControladoraProtudo.php");
	require_once("GeradoraRespostaComSlim.php");

	$app = new Slim();
	
	$app->post('/produtos', function() use( $app ){
		$params = $app->request->post();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProtudo( $geradoraResposta, $params );
		$controladora->adicionar();
	});

	$app->get('/produtos',function()use( $app ){
		$params = $app->request->get();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta , $params );
		$controladora->todos();
	});

	$app->delete('/produtos',function()use( $app ){
		$params = $app->request->delete();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta , $params );
		$controladora->remover();
	});
	

?>