
	var criarOpcoesDeValidacao = function( modoAlteracao  , obj ) {
		var opcoes = {
			focusInvalid: false,
			onkeyup: false,
			onfocusout: true,
			errorElement: "div",
			errorPlacement: function( error, element ) {
				error.appendTo( "div#msg" );
			}, 
			rules :{
				"nome":{
					required : true,
					rangelength : [2,30]
				},
				"preco":{
					required : true
				}
			},
			messages :{
				"nome":{	
					required : "O nome e obrigatorio",
					rangelength : "O nome deve ter entre 2 e 30 caracteres"
				},
				"preco":{
					required :" O preco e obrigatorio"
				}
			}
		}


		opcoes.submitHandler = function submitHandler( form ) {
				var controlesHabilitados = function ( b ){
					$( '#produto input' ).prop( "disabled", !b );
					$( '#salvar' ).prop( "disabled", !b );
					$( '#cancelar').prop( "disabled", !b);
				};

				controlesHabilitados( false );

				var sucesso = function sucesso( data , textStatus , jqXHR){
					toastr.success( 'Sucesso' );
					
				};

				var erro = function erro( data , textStatus , errorThrown){
					var mensagem = jqXHR.responseText;
					$("#msg").append( '<div class = "error" > '+ mensagem+'</div>');
				};

				var terminado = function(){
					controlesHabilitados( true );
				};
			var servicoProduto = new app.ServicoProduto();	
			console.log( obj.nome );
			var jqXHR = modoAlteracao ? servicoProduto.atualizar( obj ) : servicoProduto.adicionar( obj );
			jqXHR.done( sucesso )
				 .fail( erro )	
				 .always( terminado )
			;
		}; 	

		return opcoes;

		
}