



var app = {};


( function( app , $ ){
	'use strict';
	
	function ServicoProduto(){
	
		var _this= this ;
		
		_this.rota = function rota(){
			return 'produtos';
		}
		
		_this.criar = function criar( id , nome ,preco ){
			return{
				id : id || 0,
				nome : nome || '',
				preco : preco || 0.0
			};
		};
		
		_this.adicionar = function adicionar( obj ){
			return $.ajax({
				type : 'POST',
				url  : _this.rota(),
				data : obj
			});
		};
		
		_this.atualizar = function atualizar( obj ){
			return $.ajax({
				type : 'PUT',
				url  : _this.rota() + '/'+ obj.id,
				data : obj
			});
		};
		
		_this.remover = function remover( id ){
			return $.ajax({
				type : 'DELETE',
				url  : _this.rota() + '/' + id
			});
		};
		
		_this.comId = function comId( id ){
			return $.ajax({
				type : 'GET' ,
				url  : _this.rota() + '/' + id 
			});
		};
		
		_this.todos = function todos(){
			return $.ajax({
				type  : 'GET',
				url   : _this.rota() 
			});
		};
	};

	app.ServicoProduto = ServicoProduto ;

})( app , $ );