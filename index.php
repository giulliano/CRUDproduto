<?php
	require_once ('composer/vendor/autoload.php');
	require_once(  '/produto/ControladoraProduto.php');
	require_once( 'GeradoraRespostaComSlim.php');
	$app = new \Slim\Slim();

	$app->map( '/:x+', function( $x ) use ( $app ) {
		$app->response->header( 'Access-Control-Allow-Methods', 'HEAD, GET, POST, PATCH, PUT, DELETE, OPTIONS' );
    	$app->response->setStatus( 200 );
	} )->via( 'OPTIONS' );

	$app->post('/produtos', function() use( $app ){
		$params = $app->request->post();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta, $params );
		$controladora->adicionar();
	});

	$app->get('/produtos',function()use( $app ){
		$params = $app->request->get();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta , $params );
		$controladora->todos();
	});

	$app->delete('/produtos/:id',function( $id )use( $app ){
		$params = array("id"=>$id);
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta , $params );
		$controladora->remover();
	});

	$app->get('/produtos/:id',function( $id )use( $app){
		$params = array("id"=>$id);
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta , $params );
		$controladora->comId();
	});

	$app->put( '/produtos/:id', function( $id ) use ( $app ) {
		$params = $app->request->put();
		$geradoraResposta = new GeradoraRespostaComSlim( $app );
		$controladora = new ControladoraProduto( $geradoraResposta, $params );
		$controladora->atualizar();
	} );


	$app->run();

	

?>